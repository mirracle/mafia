import json
import random

from channels.generic.websocket import AsyncWebsocketConsumer
from channels.db import database_sync_to_async
from django.db import transaction

from game.models import GameRoom, GameUser


class ChatConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        """Подключение пользователя"""
        self.room_name = self.scope['url_route']['kwargs']['lobby_name']
        self.user_name = self.scope['url_route']['kwargs']['user_name']
        self.room_group_name = 'lobby_%s' % self.room_name
        await self.set_channel_name()
        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name
        )
        await self.accept()

    async def disconnect(self, close_code):
        """Потеря связи"""
        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )

    # Получние сигнала от WS
    async def receive(self, text_data):
        text_data_json = json.loads(text_data)
        message_type = text_data_json['type']
        message = f"{self.user_name}: {text_data_json['message']}"
        if message_type == 'chat_message':
            await self.save_chat_message(message)
        elif message_type == 'chat_mafia':
            await self.save_chat_mafia(message)
        elif message_type == 'start_game':
            message_type = 'reload'
            await self.start_game_lobby()
        elif message_type == 'day_start':
            message_type = 'reload'
            await self.start_game_day()
        elif message_type == 'night_start':
            message_type = 'reload'
            await self.start_game_night()
        elif message_type == 'kill_user':
            message_type = 'reload'
            await self.kill_user(text_data_json['message'])

        # Отправка сообщения в группу
        await self.channel_layer.group_send(
            self.room_group_name,
            {
                'type': message_type,
                'message': message
            }
        )

    # Receive message from room group
    async def chat_message(self, event):

        # Send message to WebSocket
        await self.send(text_data=json.dumps({
            'message': event
        }))

    async def chat_mafia(self, event):

        # Send message to WebSocket
        await self.send(text_data=json.dumps({
            'message': event
        }))

    async def reload(self, event):

        # Send message to WebSocket
        await self.send(text_data=json.dumps({
            'message': event
        }))

    @database_sync_to_async
    @transaction.atomic
    def set_channel_name(self):
        room = GameRoom.objects.get(room_name=self.room_name)
        user = GameUser.objects.get(room=room, user_name=self.user_name)
        user.channel_name = self.channel_name
        user.save()

    @database_sync_to_async
    @transaction.atomic
    def save_chat_message(self, message):
        room = GameRoom.objects.get(room_name=self.room_name)
        if room.chat:
            room.chat += f'{message}\n'
        else:
            room.chat = f'{message}\n'
        room.save()

    @database_sync_to_async
    @transaction.atomic
    def save_chat_mafia(self, message):
        room = GameRoom.objects.get(room_name=self.room_name)
        if room.mafia_chat:
            room.mafia_chat += f'{message}\n'
        else:
            room.mafia_chat = f'{message}\n'
        room.save()

    @database_sync_to_async
    @transaction.atomic
    def start_game_lobby(self):
        room = GameRoom.objects.get(room_name=self.room_name)
        users = room.users.filter(user_type__in=[2, 3])
        random_ids = random.sample(range(0, 7), 2)
        for random_id in random_ids:
            user = users[random_id]
            user.user_type = 3
            user.save()
        room.room_status = 2
        room.save()

    @database_sync_to_async
    @transaction.atomic
    def start_game_day(self):
        room = GameRoom.objects.get(room_name=self.room_name)
        room.game_time = 1
        room.save()

    @database_sync_to_async
    @transaction.atomic
    def start_game_night(self):
        room = GameRoom.objects.get(room_name=self.room_name)
        room.game_time = 2
        room.save()

    @database_sync_to_async
    @transaction.atomic
    def kill_user(self, message):
        user = GameUser.objects.get(user_name=message)
        user.user_status = 2
        user.save()
