from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('register/<str:lobby_name>/', views.register, name='register'),
    path('game/<str:lobby_name>/<str:user_name>/', views.lobby, name='lobby'),
]
