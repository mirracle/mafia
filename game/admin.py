from django.contrib import admin

from game.models import GameRoom, GameUser

admin.site.register(GameRoom)
admin.site.register(GameUser)
