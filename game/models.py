from django.db import models


class RoomStatus(models.IntegerChoices):
    """Статус игры"""
    PRE_GAME = 1
    IN_GAME = 2
    END_GAME = 3


class RoomTimeStatus(models.IntegerChoices):
    """Город засыпает, просыпаеться мафия"""
    DAY = 1
    NIGHT = 2


class UserStatus(models.IntegerChoices):
    """Статус игрока"""
    LIVE = 1
    DEAD = 2


class UserType(models.IntegerChoices):
    """Тип игрока"""
    LEADER = 1
    CITIZEN = 2
    MAFIA = 3


class GameRoom(models.Model):
    """Модель игровой комнаты"""
    room_name = models.CharField('Название лобби', max_length=128)
    room_status = models.IntegerField('Статус лобби', default=1, choices=RoomStatus.choices)
    game_time = models.IntegerField('Время суток в игре', default=1, choices=RoomTimeStatus.choices)
    chat = models.TextField('Чат', blank=True, null=True)
    mafia_chat = models.TextField('Чат мафии', blank=True, null=True)

    def __str__(self):
        return f'{self.room_name}'


class GameUser(models.Model):
    """Модель игрока"""
    user_name = models.CharField('Имя игрока', max_length=128)
    user_status = models.IntegerField('Статус пользователя', default=1, choices=UserStatus.choices)
    user_type = models.IntegerField('Тип пользователя', default=1, choices=UserType.choices)
    room = models.ForeignKey(GameRoom, models.CASCADE, related_name='users')
    channel_name = models.CharField('Имя приватного канала', max_length=256, null=True, blank=True)

    def __str__(self):
        return f'{self.user_name}'
