from django.shortcuts import render, redirect

from game.models import GameRoom, GameUser


def index(request):
    """Главная страница. Выбор комнаты"""
    return render(request, 'game/index.html')


def register(request, lobby_name):
    """Страница с регистрации. Выбор имени"""
    if request.GET.get('leading'):
        GameRoom.objects.create(room_name=lobby_name)
        return render(request, 'game/register.html', {'lobby_name': lobby_name})
    if not GameRoom.objects.filter(room_name=lobby_name).exists():
        request.session['error'] = 'Комната не найдена'
        return redirect('index')
    return render(request, 'game/register.html', {'lobby_name': lobby_name})


def create_user(leader, room, user_name):
    """Создать пользователя"""
    if GameUser.objects.filter(user_name=user_name, room=room).exists():
        return GameUser.objects.get(user_name=user_name, room=room)
    if leader == 'true':
        return GameUser.objects.create(user_name=user_name, room=room)
    else:
        return GameUser.objects.create(user_name=user_name, room=room, user_type=2)


def register_user(room, user_name):
    """Найти пользователя"""
    if GameUser.objects.filter(user_name=user_name, room=room).exists():
        return GameUser.objects.get(user_name=user_name, room=room)
    return False


def lobby(request, lobby_name, user_name):
    """Команаты игры"""
    leader = request.GET.get('leading')
    try:
        room = GameRoom.objects.get(room_name=lobby_name)
        user = False
        if room.room_status == 1:
            if room.users.count() >= 9 and not GameUser.objects.filter(user_name=user_name, room=room).exists():
                request.session['error'] = 'Игра уже началась'
                return redirect('index')
            user = create_user(leader, room, user_name)
        elif room.room_status == 2:
            user = register_user(room, user_name)
        if not user:
            request.session['name_error'] = 'Игра уже началась'
            return redirect('register', lobby_name=lobby_name)
        return render(request, 'game/lobby.html', {'lobby': room, 'user': user})
    except GameRoom.DoesNotExist:
        request.session['error'] = 'Комната не найдена'
        return redirect('index')
